#!/bin/bash

die(){
    echo error: $@
    exit
}

prefix=$HOME/.vimperator
rcfile=$HOME/.vimperatorrc

mkdir "$prefix" || die $prefix already exist

for configs in "colors notifier plugin";
do
    cp -r $configs $prefix || die $configs already exist
done

[[ -f $rcfile ]] && die $rcfile already exist
cp config $rcfile

# wget
cd $prefix/plugin
curl http://pinyin-hints-vimperator.googlecode.com/files/pinyin-hints.js -o pinyin-hints.js
curl https://raw.github.com/vimpr/vimperator-plugins/master/_smooziee.js -o _smooziee.js
