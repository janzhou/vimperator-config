#!/bin/bash

prefix=$HOME/.vimperator
rcfile=$HOME/.vimperatorrc


[[ -d $prefix ]] && rm -rf $prefix
[[ -f $rcfile ]] && rm -f $rcfile
